#include "tokeniser.h"
#include <string.h>
#include <stdio.h>

void tokenise( char *buf_st,
               const char *delimiter_st,
               char **words_st_pt,
               size_t n){
    char *new_start_st;
    char *token_st;
    char *strtok_save_st;
 
    for (new_start_st= buf_st;
         n>0;
         new_start_st=NULL, words_st_pt++, n--
         ){
        token_st=strtok_r(new_start_st, delimiter_st, &strtok_save_st);
        (*words_st_pt)=token_st;
        if (token_st == NULL) return;
    }
    (*words_st_pt)=NULL;
}

void print_tokens(char *const*words_st_pt){
    for (;*words_st_pt != NULL; words_st_pt++){
        fprintf(stderr,"%s » ", *words_st_pt);
    }
    fprintf(stderr,"\n");
}
