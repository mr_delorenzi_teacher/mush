default: mush

objs:=mush.o tokeniser.o process.o

flags := -Wall #-Werror #-Wextra
dep_flags = -MMD -MP

-include $(objs:.o=.d)

mush: ${objs}
	gcc -o $@ $^

%.o: %.c
	gcc ${flags} ${dep_flags} -c -o $@ $<

clean:
	rm *.o *.d mush
