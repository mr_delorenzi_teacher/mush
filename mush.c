#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

#include "process.h"
#include "tokeniser.h"

/*
  Look at process.c and tokonise.c first
  process.c has the basics of creating processes, and running programs:
     fork, exec, wait
  here we also have:
     pipe, dup, close
  You must not forget to close. If a pipe is held open by another process, then it will not close when we want it to.

  Note: the pipe is not part of the process, so is not forked. The process has references to the pipe, these are forked. When all references to the input to the pipe are closed, then the reader will recieve EOF.
*/

typedef void (*run_method)(char *phrase_st);
void run_pipe(char *phrase_st);

void run_line(char *buf_st){
    char *phrase_st_v[10];
    const size_t sizeof_phrase_st_v=
        sizeof(phrase_st_v)/sizeof(*phrase_st_v)-1;
    tokenise(buf_st, "\n;", phrase_st_v, sizeof_phrase_st_v);
    print_tokens(phrase_st_v);

    for(char **phrase_st_pt = phrase_st_v;
        *phrase_st_pt != NULL;
        phrase_st_pt++){
        run_pipe(*phrase_st_pt);
    }
}

void run_pipe(char *buf_st){
    int number_of_children=0;
    char *phrase_st_v[10];
    const size_t sizeof_phrase_st_v=
        sizeof(phrase_st_v)/sizeof(*phrase_st_v)-1;
    tokenise(buf_st, "|", phrase_st_v, sizeof_phrase_st_v);
    fprintf(stderr,"processing_pipe:");
    print_tokens(phrase_st_v);

    pid_t pid;
    int left_pipefd[2]={-1,-1};
    int right_pipefd[2]={-1,-1};
    for(char **phrase_st_pt = phrase_st_v;
        *phrase_st_pt != NULL;
        phrase_st_pt++,
            left_pipefd[0] = right_pipefd[0],
            left_pipefd[1] = right_pipefd[1],
            right_pipefd[0]=-1,
            right_pipefd[1]=-1,
            number_of_children++
        ){

        if ( *(phrase_st_pt+1) != NULL ){
            fprintf(stderr,"pipe:\n");
            if (pipe(right_pipefd) == -1) {
               perror("pipe");
               exit(EXIT_FAILURE);
            }
        }
        pid= fork();
        if (pid== -1){
            perror("fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0){
            //child
            fprintf(stderr,"child:%s\n", *phrase_st_pt);

            if (left_pipefd[1] != -1){
                close(left_pipefd[1]);
            }

            if (left_pipefd[0] != -1){
                if (-1 == dup2(left_pipefd[0],STDIN_FILENO)){
                    perror("dup");
                    exit(EXIT_FAILURE);
                }
                close(left_pipefd[0]);
            }

            if (right_pipefd[0] != -1){
                close(left_pipefd[0]);
            }
            if (right_pipefd[1] != -1){
                if (-1 == dup2(right_pipefd[1],STDOUT_FILENO)){
                    perror("dup");
                    exit(EXIT_FAILURE);
                }
                close(right_pipefd[1]);
            }

            fprintf(stderr,"exec:%s\n", *phrase_st_pt);
            exec_process(*phrase_st_pt);
        }
        close(left_pipefd[0]);
        close(left_pipefd[1]);

    }
    fprintf(stderr,"waiting:\n");
    for (int n=0; n<number_of_children; n++){
        fprintf(stderr,"wait:\n");
        int status;
        waitpid(-1,&status,0);
    }
    fprintf(stderr,"done:\n");
}

int main(int argc, char *argv[]) {
    char buf_v[100];

    while ( NULL != fgets(buf_v,sizeof(buf_v)-1,stdin) ){
        run_line(buf_v);
    }
}
