#include <stddef.h>

void tokenise( char *buf_st, //mutated
               const char *delimiter_st,
               char **words_st_pt, //output
               size_t max_size);

void print_tokens(char *const*words_st_pt);
