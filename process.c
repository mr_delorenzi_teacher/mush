#include "process.h"

#include "tokeniser.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

pid_t run_process(char *buf_st){
    pid_t pid= fork();
    if (pid== -1){
        fprintf(stderr,"Error: can't fork\n");
        exit(-1);
    } else if (pid == 0){
        //child
        exec_process(buf_st);
    }

    return pid;
}

void exec_process(char *buf_st){
    char *words_st_v[10];
    const size_t sizeof_words_st_v=
        sizeof(words_st_v)/sizeof(*words_st_v)-1;

    tokenise(buf_st, " ", words_st_v, sizeof_words_st_v);

    execvp(words_st_v[0], words_st_v);
}

void run_process_and_wait (char *phrase_st){
    const pid_t pid = run_process(phrase_st);
    int status;
    waitpid(pid,&status,0);
}
